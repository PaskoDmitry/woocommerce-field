<?php
/*
Plugin Name: WooCommerce Field
Description: Add Field in check out
Version: 1.0
Author: Pasko Dmitry
*/


/*  Copyright 2017  Pasko_dmitry  (email: PaskoDima@meta.ua)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );

/**
 * Adding a field to the checkout page
 */
function my_custom_checkout_field( $checkout ) {
	echo '<div id="my_custom_checkout_field"><h3>' . __( 'Additional Information' ) . '</h3>';

	woocommerce_form_field( 'add_info_name', array(
		'type'        => 'text',
		'class'       => array( 'my-field-class form-row-wide' ),
		'label'       => __( 'Fill in this field *' ),
		'placeholder' => __( 'Enter something' ),
	), $checkout->get_value( 'add_info_name' ) );
	echo '</div>';
}

/**
 * Executing the order form
 */
add_action( 'woocommerce_checkout_process', 'my_custom_checkout_field_process' );
function my_custom_checkout_field_process() {
	// Check if the field is full, if not, add an error.
	if ( ! $_POST['add_info_name'] ) {
		wc_add_notice( __( 'Please enter something into this new shiny field.' ), 'error' );
	}
}

/**
 * Update order metadata with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
	if ( ! empty( $_POST['add_info_name'] ) ) {
		update_post_meta( $order_id, 'Additional Information', sanitize_text_field( $_POST['add_info_name'] ) );
	}
}

/**
 * Display the value of the field on the edit order page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta( $order ) {
	echo '<p><strong>' . __( 'Additional Information' ) . ':</strong> ' . get_post_meta( $order->id, 'Additional Information', true ) . '</p>';
}

/**
 * Adding a field to the order letters
 **/
add_filter( 'woocommerce_email_order_meta_keys', 'my_custom_checkout_field_order_meta_keys' );

function my_custom_checkout_field_order_meta_keys( $keys , $order) {
	$keys['Additional Information'] = get_post_meta( $order->id, 'Additional Information', true );
	return $keys;
}

